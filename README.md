# AssetAPI #

The AssetsAPI is a centralized API endpoint that handles all asset creation.

Refer Installation, Testing, and Api usage, and Settings to the [wiki](https://bitbucket.org/scooblyboo/assetapi/wiki/Home)
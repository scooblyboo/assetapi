<?php

return [
    'image' => [
        'asset_type' => AssetType::IMAGE
    ],
    'video' => [
        'asset_type' => AssetType::VIDEO
    ],
    'file' => [
        'asset_type' => AssetType::FILE
    ]
];